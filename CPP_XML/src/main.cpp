#include <iostream>
#include <conio.h>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>
#include <list>
#include <cstdlib>
#include "../class/class.h"
#include "../class/student.h"
#include "tinyxml2.h"
#define NAME_SIZE 100

using namespace std;

namespace patch
{
    template <typename T> std::string to_string(const T& n)
    {
        std::ostringstream stm;
        stm << n;
        return stm.str();
    }
    
}

//Structure
typedef struct{
    Student* student;
    list<Class*> classes;
}DataSelected;

typedef struct seg{
    string parent;
    list<struct seg> children;
}Node;

//Function prototype
void clearBuffer();
void createFile(char*, string);

void createStudent(list<Student*>*);
void createClass(list<Class*>*);

void readStudent(list<Student*>*);
void readClass(list<Class*>*);

void selectData(list<DataSelected>*, list<Student*>, list<Class*>);
Student* getStudent(list<Student*>, int);
Class* getClass(list<Class*>, int);
void displayDataSelected(list<DataSelected>);

bool isStudentExist(list<DataSelected>, int);
bool isClassExist(list<Class*>, int);

std::string selectSegment(list<Node>*);
void searchXmlName(tinyxml2::XMLElement*, list<Node>*);

bool createXML(list<DataSelected>, list<Node>);
//void createFirstSegment(char*, list<DataSelected>);
//void createSecondSegment(char*, list<DataSelected>);

tinyxml2::XMLElement* buildNode(tinyxml2::XMLDocument, list<DataSelected>, list<Node>);
tinyxml2::XMLElement* buildStudent(tinyxml2::XMLDocument, DataSelected, list<Node>, int);
tinyxml2::XMLElement* buildClass(tinyxml2::XMLDocument, Class*, list<Node>, int);

void displayStudent(list<Student*>);
void displayClass(list<Class*>);
void displaySeg(list<Node>);

int strtoi(string);

Student* convertStringToStudent(string);
Class* convertStringToClass(string);

Student* saveStudent();
Class* saveClass();

string convertStudentToString(Student*);
string convertClassToString(Class*);

int getAI(char*);
void saveAI(char*, int);

string retStudentData(Student*,string);
string retClassData(Class*,string);

//Gloabl variables
int student_id;
int class_id;
char studentFileLocation[] = "../database/students.txt";
char classFileLocation[] = "../database/classes.txt";
char aiStudentFileLocation[] = "../database/AIstudent.txt";
char aiClassFileLocation[] = "../database/AIclass.txt";

//void create(){
//    tinyxml2::XMLDocument doc;
//    tinyxml2::XMLElement* elem = doc.NewElement("hello");
//    elem->LinkEndChild(doc.NewText("world"));
//    doc.LinkEndChild(doc.NewDeclaration());
//    doc.LinkEndChild(elem);
//    doc.SaveFile("../xml/b.xml");
//}

//Functions
int main(int argc, const char * argv[]) {
    list<Student*> studentList;
    list<Class*> classList;
    list<DataSelected> data;
    list<Node> segment;
    std::string segmentFile;

    //    printf("Path relative to the working directory is: %s\n", argv[0]);

    int ans;
    bool isSaved = false;
    
    student_id = getAI(aiStudentFileLocation);
    class_id = getAI(aiClassFileLocation);
    readStudent(&studentList);
    readClass(&classList);
    data.clear();
    segment.clear();

    do{
        system("cls");

        cout << "1 - Create Student\n2 - Create Class\n3 - Read Student\n4 - Read Class\n5 - Select Data\n6 - Select Segment\n7 - Create XML\n0 - EXIT\n\n----------------------------------\n\n";
        cout << "\nAns: ";
        cin >> ans;
        clearBuffer();
        system("cls");

        switch(ans){
            case 1: createStudent(&studentList);break;
            case 2: createClass(&classList);break;
            case 3: displayStudent(studentList);break;
            case 4: displayClass(classList);break;
            case 5: selectData(&data, studentList, classList);break;
            case 6: segmentFile = selectSegment(&segment);displaySeg(segment);break;
            case 7: isSaved = createXML(data,segment);break;
            case 0: cout << "\n\nExiting...";break;
            default: continue;
        }
        
        if(isSaved){
            data.clear();
            segment.clear();
            isSaved = false;
        }
        
        cout << "\n\nPress any key to continue...";
        getch();

    }while(ans != 0);
    
    
    return 0;
}

void displaySeg(list<Node> lt){
    list<Node>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end(); iter++){
        cout << "\nparent is:" << (*iter).parent << ", children size: " << (*iter).children.size();
        displaySeg((*iter).children);
    }
}

int getAI(char* name){
    int ret = 0;
    FILE* file;
    file = fopen(name, "r");
    
    if(file != NULL){
        fscanf (file, "%d", &ret);
    }
    
    fclose(file);
    
    return ret;
}

void clearBuffer(){
    char ch;
    while ((ch = cin.get()) != '\n' && ch != EOF);
}

void saveAI(char* name, int id){
    FILE* file;
    char buffer[NAME_SIZE];
    
    file = fopen(name, "w");
    
    if(file == NULL){
        perror("Error opening file");
    }else{
        sprintf(buffer, "%d", id);
        fputs(buffer, file);
        
        fclose(file);
    }
}

void createFile(char* fileName, string content){
    ofstream file (fileName, ios_base::app);
    
    if (file.is_open()){
        file << content;
        file.close();
    }else{
        cout << "Unable to open file";
    }
}

void createStudent(list<Student*>* lt){
    Student* s;
    
    s = saveStudent();
    createFile(studentFileLocation, convertStudentToString(s));
    saveAI(aiStudentFileLocation, student_id);
    readStudent(lt);
}

void createClass(list<Class*>* lt){
    Class* c;
    
    c = saveClass();
    createFile(classFileLocation, convertClassToString(c));
    saveAI(aiClassFileLocation, class_id);
    readClass(lt);
}

void readStudent(list<Student*>* lt){
    string line;
    ifstream file(studentFileLocation);
    if(file.is_open()){
        lt->clear();
        while(getline(file, line)){
            lt->push_back(convertStringToStudent(line));
        }
        file.close();
    }else{
        cout << "Unable to open file";
    }
}

void readClass(list<Class*>* lt){
    string line;
    ifstream file(classFileLocation);
    if(file.is_open()){
        lt->clear();
        while(getline(file, line)){
            lt->push_back(convertStringToClass(line));
        }
        file.close();
    }else{
        cout << "Unable to open file";
    }
}

Student* convertStringToStudent(string line){
    Student* s = new Student();
    size_t pos = 0, ppos = 0;
    string token, ttoken, delimiter = ", ";
    
    while((pos = line.find(delimiter)) != std::string::npos){
        token = line.substr(0, pos);
        
        ppos = token.find(":");
        ttoken = token.substr(ppos+1, token.size());
        s->setId(strtoi(ttoken));
        line.erase(0, pos + delimiter.length());
    }
    
    ppos = line.find(":");
    ttoken = line.substr(ppos+1, line.size());
    s->setName(ttoken);
    
    return s;
}

Class* convertStringToClass(string line){
    Class* c = new Class();
    size_t pos = 0, ppos = 0;
    string token, ttoken, delimiter = ", ";
    int ctr = 0;
    
    while((pos = line.find(delimiter)) != std::string::npos){
        token = line.substr(0, pos);
        
        ppos = token.find(":");
        ttoken = token.substr(ppos+1, token.size());
        
        switch(ctr){
            case 0: c->setId(strtoi(ttoken));break;
            case 1: c->setDescription(ttoken);break;
            case 2: c->setSchedule(ttoken);break;
        }
        
        line.erase(0, pos + delimiter.length());
        ctr ++;
    }
    
    ppos = line.find(":");
    ttoken = line.substr(ppos+1, line.size());
    c->setRoom(ttoken);
    
    return c;
}

int strtoi(string s){
    int ret;
    std::istringstream ss(s);
    ss >> ret;
    return ret;
}

void selectData(list<DataSelected>* datas, list<Student*> students, list<Class*> classes){
    int id;
    Student* s_buff;
    Class* c_buff;
    DataSelected d_buff;
    
    displayStudent(students);
    
    cout << "\nType student id to select: ";
    cin >> id;
    
    s_buff = getStudent(students, id);
    cout << "\n";
    
    if(s_buff == NULL){
        cout << "Student Not Found!";
    }else if(isStudentExist(*datas, id)){
        cout << "Student Already Added.";
    }else{
        d_buff.student = s_buff;
        
        do{
            system("cls");
            displayClass(classes);
            
            cout << "\nType class id to select(-1 - Exit): ";
            cin >> id;
            
            if(id != -1){
                c_buff = getClass(classes, id);
                
                if(c_buff == NULL){
                    cout << "Class Not Found!";
                    cout << "\n\nPress any key...";
                    getch();
                }else{
                    if(!d_buff.classes.empty() && isClassExist(d_buff.classes, id)){
                        cout << "Class Already Added.";
                        cout << "\n\nPress any key...";
                        getch();
                    }else{
                        d_buff.classes.push_back(c_buff);
                    }
                }
            }
        }while(id != -1);
        
        datas->push_back(d_buff);
    }
    
}

Student* getStudent(list<Student*> lt, int id){
    list<Student*>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end() && (*iter)->getId() != id; iter++){}
    
    return (iter != lt.end())? (*iter) : NULL;
}

Class* getClass(list<Class*> lt, int id){
    list<Class*>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end() && (*iter)->getId() != id; iter++){}
    
    return (iter != lt.end())? (*iter) : NULL;
}

bool isStudentExist(list<DataSelected> lt, int id){
    list<DataSelected>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end() && (*iter).student->getId() != id; iter++){}
    
    return (iter != lt.end());
}

bool isClassExist(list<Class*> lt, int id){
    list<Class*>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end() && (*iter)->getId() != id; iter++){}
    
    return (iter != lt.end());
}

std::string selectSegment(list<Node>* segment){
    char fileName[NAME_SIZE];
    
    tinyxml2::XMLDocument xmlDoc;
    tinyxml2::XMLError xmlError;
    tinyxml2::XMLElement* xmlElem;
    
    cout << "Input File Name: ";
    scanf("%s", fileName);
    
    char name [strlen(fileName) + 10];
    
    strcpy(name, "../structure/");
    strcat(name, fileName);
    
    xmlError = xmlDoc.LoadFile(name);
    
    if(xmlError == 0){
        segment->clear();
        xmlElem = xmlDoc.FirstChildElement();
        if(xmlElem){
            searchXmlName(xmlElem, segment);
        }else{
            cout << "No root node";
        }
        
    }else{
        cout << "Segment name Not Found!";
    }
    
    return (xmlError == 0)? name : "";
}

void searchXmlName(tinyxml2::XMLElement* root, list<Node>* segment){
    tinyxml2::XMLHandle hroot(0);
    tinyxml2::XMLElement* xmlElem = root;
//    tinyxml2::XMLAttribute* xmlAttrib = NULL;
    
    char* tempNode;
//    char* tempText;
    
    hroot = tinyxml2::XMLHandle(xmlElem);
    xmlElem = hroot.FirstChildElement().ToElement();
    if(xmlElem){
        while(xmlElem){
            Node node;
            node.children.clear();
            
            tempNode = (char*)xmlElem->Value();
            if(tempNode){
                printf("Node: <%s>\n", tempNode);
                node.parent = tempNode;
            }
//            tempText = (char*)xmlElem->GetText();
//            if(tempText)
//                printf("Text: %s\n", tempText);
            searchXmlName(xmlElem, &(node.children));
            segment->push_back(node);
            xmlElem = xmlElem->NextSiblingElement();
            
        }
    }
    
}

bool createXML(list<DataSelected> lt, list<Node> segment){
    char fileName[NAME_SIZE];
    int ctr = 0;
    bool ret = false;
    
    list<Node>::iterator iter;
    list<DataSelected>::iterator d_iter;
    
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLDeclaration* decl = doc.NewDeclaration();
    tinyxml2::XMLElement* root = doc.NewElement("students");
    
    if(lt.empty()){
        cout << "No data Selected!";
    }else if(segment.empty()){
        cout << "No segment Chosen!";
    }else{
        cout << "Input File Name: ";
        scanf("%s", fileName);
        
        char name [strlen(fileName) + 10];
        strcpy(name, "../xml/");
        strcat(name, fileName);
        cout << name;
        
        doc.LinkEndChild(decl);
        doc.LinkEndChild(root);
        
        for(iter = segment.begin(); iter != segment.end(); iter++){
            if((*iter).parent == "student"){
                
                for(d_iter = lt.begin(); d_iter != lt.end(); d_iter++){
                    //Build student
                    tinyxml2::XMLElement* s_elem;
                    tinyxml2::XMLElement* child;
                    list<Node> s_segment = (*iter).children;
                    list<Node>::iterator s_node_iter;
                    
                    int c_ctr = 0;
                    char temp[] = "student";
                    char str[strlen(temp) + 5];
                    DataSelected data = (*d_iter);
                    
                    strcpy(str, temp);
                    
                    if(ctr < 10){
                        strcat(str, "0");
                    }
                    
                    strcat(str, patch::to_string(ctr).c_str());
                    s_elem = doc.NewElement(str);
                    root->LinkEndChild(s_elem);
                    
                    for(s_node_iter = s_segment.begin(); s_node_iter != s_segment.end(); s_node_iter++){
                        if((*s_node_iter).parent == "class"){
                            list<Class*>::iterator c_iter;
                            list<Class*> classes = data.classes;
                            
                            for(c_iter = classes.begin(); c_iter != classes.end(); c_iter++){
                                //Build Class
                                tinyxml2::XMLElement* c_elem;
                                tinyxml2::XMLElement* c_child;
                                list<Node> c_segment = (*s_node_iter).children;
                                list<Node>::iterator c_node_iter;
                                
                                char c_temp[] = "class";
                                char c_str[strlen(c_temp) + 5];
                                
                                strcpy(c_str, c_temp);
                                
                                if(c_ctr < 10){
                                    strcat(c_str, "0");
                                }
                                
                                strcat(c_str, patch::to_string(c_ctr).c_str());
                                
                                c_elem = doc.NewElement(c_str);
                                s_elem->LinkEndChild(c_elem);
                                
                                for(c_node_iter = c_segment.begin(); c_node_iter != c_segment.end(); c_node_iter++){
                                    string c_temp_str = retClassData((*c_iter), (*c_node_iter).parent);
                                    
                                    char c_temp_text[c_temp_str.size()+1];
                                    char c_temp_name[(*c_node_iter).parent.size()+1];
                                    
                                    strcpy(c_temp_text, c_temp_str.c_str());
                                    strcpy(c_temp_name, (*c_node_iter).parent.c_str());
                                    
                                    c_child = doc.NewElement(c_temp_name);
                                    c_child->LinkEndChild(doc.NewText(c_temp_text));
                                    c_elem->LinkEndChild(c_child);
                                }
                                
                                c_ctr ++;
                            }
                        }else{
                            string temp_str = retStudentData(data.student, (*s_node_iter).parent);
                            
                            char temp_text[temp_str.size()+1];
                            char temp_name[(*s_node_iter).parent.size()+1];
                            
                            strcpy(temp_text, temp_str.c_str());
                            strcpy(temp_name, (*s_node_iter).parent.c_str());
                            
                            child = doc.NewElement(temp_name);
                            child->LinkEndChild(doc.NewText(temp_text));
                            s_elem->LinkEndChild(child);
                        }
                    }
                    
                    ctr ++;
                }
            }
        }
        
        ret = true;
        doc.SaveFile(name);
    }
    
    return ret;
}


////Name , Description
//void createFirstSegment(char* fileName, list<DataSelected> lt){
//    string content;
//    Student* s;
//    Class* c;
//    list<DataSelected>::iterator iter;
//    list<Class*>::iterator c_iter;
//    
//    //	char name [strlen(fileName) + 8];
//    //	strcpy(name, "../xml/");
//    //	strcat(name, fileName);
//    //	cout << name;
//    
//    ofstream file (fileName);
//    
//    if (file.is_open()){
//        content = "<students>\n";
//        
//        for(iter = lt.begin(); iter != lt.end(); iter++){
//            s = (*iter).student;
//            
//            content += "\t<student0" + patch::to_string(s->getId()) + ">\n";
//            content += "\t\t<name>" + s->getName() + "</name>\n";
//            
//            for(c_iter = (*iter).classes.begin(); c_iter != (*iter).classes.end(); c_iter++){
//                c = (*c_iter);
//                content += "\t\t<class0" + patch::to_string(c->getId()) + ">";
//                content += "\t\t\t<description>" + c->getDescription() + "</description>";
//                content += "\t\t</class0" + patch::to_string(c->getId()) + ">";
//            }
//            
//            content += "\t</student0" + patch::to_string(s->getId()) + ">\n";
//        }
//        
//        content += "</students>";
//        
//        file << content;
//        file.close();
//    }else{
//        cout << "Unable to open file";
//    }
//}
//
////Name, Sched, Room
//void createSecondSegment(char* fileName, list<DataSelected> lt){
//    string content;
//    Student* s;
//    Class* c;
//    list<DataSelected>::iterator iter;
//    list<Class*>::iterator c_iter;
//    
//    //	char name [strlen(fileName) + 8];
//    //	strcpy(name, "../xml/");
//    //	strcat(name, fileName);
//    //	cout << name;
//    
//    ofstream file (fileName);
//    
//    if (file.is_open()){
//        content = "<students>\n";
//        
//        for(iter = lt.begin(); iter != lt.end(); iter++){
//            s = (*iter).student;
//            
//            content += "\t<student0" + patch::to_string(s->getId()) + ">\n";
//            content += "\t\t<name>" + s->getName() + "</name>\n";
//            
//            for(c_iter = (*iter).classes.begin(); c_iter != (*iter).classes.end(); c_iter++){
//                c = (*c_iter);
//                content += "\t\t<class0" + patch::to_string(c->getId()) + ">";
//                content += "\t\t\t<schedule>" + c->getSchedule() + "</schedule>";
//                content += "\t\t\t<room>" + c->getRoom() + "</room>";
//                content += "\t\t</class0" + patch::to_string(c->getId()) + ">";
//            }
//            
//            content += "\t</student0" + patch::to_string(s->getId()) + ">\n";
//        }
//        
//        content += "</students>";
//        
//        file << content;
//        file.close();
//    }else{
//        cout << "Unable to open file"; 
//    }
//}

void displayStudent(list<Student*> lt){
    list<Student*>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end(); iter++){
        (*iter)->display();
    }
}

void displayClass(list<Class*> lt){
    list<Class*>::iterator iter;
    
    for(iter = lt.begin(); iter != lt.end(); iter++){
        (*iter)->display();
    }
}

Student* saveStudent()
{
    string name;
    
    Student* s;
    
    cout << "\nEnter student name: ";
    getline(cin, name);
    
    s = new Student(student_id++, name);
    
    return s;	
}

Class* saveClass()
{
    string description;
    string schedule;
    string room;
    
    Class* c;
    
    cout << "\nEnter class description: ";
    getline(cin, description);
    
    cout << "\nEnter class schedule: ";
    getline(cin, schedule);
    
    cout << "\nEnter class room: ";
    getline(cin, room);
    
    c = new Class(class_id++, description, schedule, room);
    
    return c;
}

string convertStudentToString(Student* s)
{
    string ret;
    
    ret = "id:" + patch::to_string(s->getId());
    ret += ", name:" + s->getName();
    ret += "\n";
    
    return ret;
}

string convertClassToString(Class* c)
{
    string ret;
    
    ret = "id:" + patch::to_string(c->getId());
    ret += ", description:" + c->getDescription();
    ret += ", schedule:" + c->getSchedule();
    ret += ", room:" + c->getRoom();
    ret += "\n";
    
    return ret;
}

void displayDataSelected(list<DataSelected> lt) {
    if(lt.empty()){
        cout << "list is empty";
    }else{
        list<DataSelected>::iterator iter;
        
        for(iter = lt.begin(); iter != lt.end(); iter++){
            (*iter).student->display();
            displayClass((*iter).classes);
        }
    }
}

string retStudentData(Student* s, string str){
    string ret = "";
    
    if(str == "id"){
        ret = patch::to_string(s->getId());
    }else if(str == "name"){
        ret = s->getName();
    }
    
    return ret;
}

string retClassData(Class* c, string str){
    string ret = "";
    
    if(str == "id"){
        ret = patch::to_string(c->getId());
    }else if(str == "room"){
        ret = c->getRoom();
    }else if(str == "schedule"){
        ret = c->getSchedule();
    }else if(str == "description"){
        ret = c->getDescription();
    }
    
    return ret;
}
