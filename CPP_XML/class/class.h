#include <string>
#pragma once

using namespace std;

class Class
{
	private:
		int id;
		string description;
		string schedule;
		string room;
		
	public:
		Class(){}
		
		Class(int id, string description, string schedule, string room){
			this->id = id;
			this->description = description;
			this->schedule = schedule;
			this->room = room;
		}
		
		void setId(int id){
			this->id = id;
		}
		
		int getId(){
			return id;
		}
		
		void setDescription(string description){
			this->description = description;
		}
		
		string getDescription(){
			return description;
		}
		
		void setSchedule(string schedule){
			this->schedule = schedule;
		}
		
		string getSchedule(){
			return schedule;
		}
		
		void setRoom(string room){
			this->room = room;
		}
		
		string getRoom(){
			return room;
		}
		
		void display(){
			cout << "Class id:" << id << ", Description: " << description;
			cout << ", Schedule: " << schedule << ", Room: " << room << "\n";
		}
};
