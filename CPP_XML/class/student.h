#include <string>
#pragma once

using namespace std;

class Student
{
	private:
		int id;
		string name;
		
	public:
		Student(){}
		
		Student(int id, string name){
			this->id = id;
			this->name = name;
		}
		
		void setId(int id){
			this->id = id;
		}
		
		int getId(){
			return id;
		}
		
		void setName(string name){
			this->name = name;
		}
		
		string getName(){
			return name;
		}
		
		void display(){
			cout << "Student id:" <<id << ", Name: " << name << "\n";
		}
};
